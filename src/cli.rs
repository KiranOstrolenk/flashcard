use clap::{crate_authors, Parser};
use std::path::PathBuf;

#[derive(Parser, Debug)]
#[clap(author=crate_authors!(), version, about, name = "flashcard")]
/// A flashcard program
pub(crate) struct Options {
    /// The yaml file wherein the flashcards are defined
    #[clap(name = "FLASHCARD_YAML", parse(from_os_str))]
    pub(crate) flashcard_file: PathBuf,

    /// Specify particular topics to run through
    #[clap(name = "topic", short, long)]
    pub(crate) topics: Vec<String>,

    /// List topics within flashcard set
    #[clap(short, long)]
    pub(crate) list: bool,

    /// Filter out questions you previously marked as correct
    #[clap(short, long)]
    pub(crate) filter: bool,
}
