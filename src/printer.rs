use std::{
    collections::BTreeMap,
    io::{stdout, Write},
};

use ansi_term::Style;
use anyhow::{Context, Result};
use console::Term;

use crate::{FlashCard, ScoreCard};

pub struct Printer<'a> {
    flashcards: &'a FlashCard,
    scorecard: &'a mut ScoreCard,
    topic: Style,
    question_wrong: Style,
    question_right: Style,
    question_new: Style,
    answer: Style,
    term: Term,
}

impl<'a> Printer<'a> {
    pub fn new(flashcards: &'a FlashCard, scorecard: &'a mut ScoreCard) -> Self {
        let topic = Style::new().bold().underline();
        let question_wrong = Style::new().fg(ansi_term::Color::Red).bold();
        let question_right = Style::new().fg(ansi_term::Color::Green).bold();
        let question_new = Style::new().bold();
        let answer = Style::new().fg(ansi_term::Color::Cyan).bold();
        let term = console::Term::stdout();
        Self {
            topic,
            answer,
            term,
            flashcards,
            scorecard,
            question_wrong,
            question_right,
            question_new,
        }
    }

    pub fn print_flashcards(&mut self, filter: bool) -> Result<&ScoreCard> {
        'topic_loop: for topic in self.flashcards {
            self.print_topic(topic.0)?;
            let questions = topic.1;
            for q_and_a in questions {
                let q_status = match self.scorecard.get(topic.0) {
                    Some(t) => t.get(q_and_a.0),
                    None => None,
                };
                if q_status == Some(&true) && filter {
                    continue;
                }
                self.print_question(q_and_a.0, q_status)?;
                if self.term.read_char()?.to_ascii_lowercase() == 'q' {
                    break 'topic_loop;
                }
                self.print_answer(q_and_a.1)?;
                print!("Correct? (y/n/q)");
                stdout().flush()?;
                match self.term.read_char()?.to_ascii_lowercase() {
                    'y' => self.update_scorecard(topic.0, q_and_a.0, true),
                    'n' => self.update_scorecard(topic.0, q_and_a.0, false),
                    'q' => break 'topic_loop,
                    _ => (),
                };
                println!("\n");
            }
        }
        Ok(self.scorecard)
    }

    fn update_scorecard(&mut self, topic: &str, question: &str, mark: bool) {
        if let Some(t) = self.scorecard.get_mut(topic) {
            t.insert(question.to_owned(), mark);
        } else {
            self.scorecard.insert(
                topic.to_owned(),
                BTreeMap::from([(question.to_owned(), mark)]),
            );
        }
    }

    pub fn scorecard(&self) -> &ScoreCard {
        self.scorecard
    }

    fn print_topic(&self, raw_topic: &str) -> Result<()> {
        // Dirty
        let topic_msg = raw_topic.replace('\n', "\n   ");
        println!("{}", self.topic.paint(topic_msg));
        stdout().flush().context("Failed to flush topic printing")
    }

    fn print_question(&self, raw_question: &str, question_status: Option<&bool>) -> Result<()> {
        // Dirty
        let question_msg = raw_question.replace('\n', "\n   ");
        match question_status {
            Some(true) => print!("{} {}", self.question_right.paint("Q:"), question_msg),
            Some(false) => print!("{} {}", self.question_wrong.paint("Q:"), question_msg),
            None => print!("{} {}", self.question_new.paint("Q:"), question_msg),
        }
        stdout()
            .flush()
            .context("Failed to flush question printing")
    }

    fn print_answer(&self, raw_answer: &str) -> Result<()> {
        // Dirty
        let answer_msg = raw_answer.replace('\n', "\n   ");
        println!("\n{} {}", self.answer.paint("A:"), answer_msg);
        stdout().flush().context("Failed to flush answer printing")
    }
}
